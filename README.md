# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* OpenCV for Visual Studio to allow apps to be produced on WinRT and WinPhone 8.0/8.0 Silverlight.

### How do I get set up? ###

- Download Zip containing opencv libraries from "https://github.com/MSOpenTech/opencv" and unpack to desired path.
- Place the C# program: "program.cs" within a new solution / project in Visual Studio.
- Near the top of the program, change the string variable "root" to the path of where the opencv-winrt folder 
  (the unpacked zip folder) is located.
- Add double backslashes within the path for escape sequencing. (e.g. "C:\\Users\\ etc.")
- Run the C# program file. Once finished, the project can be closed.
- Navigate to opencv-winrt that has now been edited. Choose the platform you wish to code in (e.g. binWP8 (for Windows Phone 8), 
  binWINRT (for Windows 8)).
- Within either folder, there is an opencv.sln file which contains several projects coresponding to the several OpenCV libraries. 
  Build the solution to create libraries.
- From here you can start creating an app within the opencv.sln solution through two ways: (Choice 1 is easy for WinRT using a win32 console app, not phone)

1. Make a new project of your choice depending on platform and reference to existing opencv projects for which you require.
This is done by:
 - Creating a new project for the application within the opencv.sln solution. 
 - Right clicking your created project -> Properties -> Common Properties -> Add New Reference... -> Solution -> Projects -> Then select all the project libraries required. 
 - Go to Configuration Properties -> C/C++ -> General -> Additional Include Directories. Click the drop down arrow on the right, select "<Edit...>", 
   double click in blank space in the white box, select the "..." button and navigate to the path where the opencv-winrt folder is. 
   Then select the platform your using (binWinRT for Windows 8, binWP8 for Windows Phone 8) and press ok. A path to the platform should now be seen. Press Ok and your done.

2. Use one of the pre-existing OpenCV samples (given in the opencv-winrt folder) as a template: 

 - In your opencv.sln right click solution at the top in solution explorer -> add -> existing project 
 - Navigate to opencv-winrt -> samples and then select the platform to work on (e.g. winrt (Windows 8), winrt-universal (Windows Universal App), wp8 (Windows Phone 8)) 
 - Once a platform folder is chosen, select a sample project of your choice and retrieve all classes in that project (excluding project libraries). 
 - Right click the project you just retrieved, and go to common properties, remove the references that are there already there except for platform, 
   and add the references in your own solution. Then continue as is told halfway through the previous process above, up to the Configuration Properties step.