﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Collections;


/// <summary>
///  This class prepares the opencv-wrt folder 
/// </summary>

namespace Imersia.Tools.OpenCvParser
{
    class Program : IFileProcess
    {
        string root = "C:\\Users\\David\\Documents\\opencv-winrt\\opencv-winrt";    // Change Name of User before continuing


        static void Main(string[] args)
        {
            IFileProcess p = new Program();                         // To use the interface methods
            string[] root3rdPartyDirectories = Directory.GetDirectories(p.GetRoot3rdParty()); // Get folders of root3rdparty into an array
            string[] WinRT3rdPartyDirectories = Directory.GetDirectories(p.GetWinRT3rdParty());
            string[] WP83rdPartyDirectories = Directory.GetDirectories(p.GetWP83rdParty());

            Console.WriteLine("Working...");
            foreach (string s in WinRT3rdPartyDirectories)  //Iterate through the directories in WinRT 3rdparty
            {

                string directoryName = new DirectoryInfo(s).Name;
                p.CopyFolderFiles(p.GetRoot3rdParty() + "\\" + directoryName, s);   //copy the files from root to platform folder
                DirectoryCopy(p.GetRoot3rdParty() + "\\" + directoryName, s);   //copy the directories from root to platform folder
                string[] WinRT3rdPartyFiles = Directory.GetFiles(s);
                foreach (string s1 in WinRT3rdPartyFiles)   //Iterate through the directories in WinRT 3rdparty
                {
                    if (Path.GetExtension(s1).Equals(".vcxproj") || Path.GetExtension(s1).Equals(".filters"))
                    {
                        p.EditFile(s1, "thirdParty", directoryName, "WinRT");

                    }
                }

            }
            Console.WriteLine("WinRt 3rdparty Ready");

            foreach (string s in WP83rdPartyDirectories)
            {          

                string directoryName = new DirectoryInfo(s).Name;
                p.CopyFolderFiles(p.GetRoot3rdParty() + "\\" + directoryName, s);   //copy the files from root to platform folder
                DirectoryCopy(p.GetRoot3rdParty() + "\\" + directoryName, s);   //copy the directories from root to platform folder
                string[] WP83rdPartyFiles = Directory.GetFiles(s);
                foreach (string s1 in WP83rdPartyFiles) //Iterate through the directories in WP8 3rdparty
                {
                    if (Path.GetExtension(s1).Equals(".vcxproj") || Path.GetExtension(s1).Equals(".filters"))
                    {
                        p.EditFile(s1, "thirdParty", directoryName, "WP8");

                    }
                }
            }
            Console.WriteLine("WP8 3rdparty Ready");

            string[] rootModulesDirectories = Directory.GetDirectories(p.GetRootModules()); // Get folders of rootModules into an array
            string[] WinRTModulesDirectories = Directory.GetDirectories(p.GetWinRTModules());
            string[] WP8ModulesDirectories = Directory.GetDirectories(p.GetWP8Modules());
            foreach (string s in WinRTModulesDirectories)
            {
                string directoryName = new DirectoryInfo(s).Name;
                p.CopyFolderFiles(p.GetRootModules() + "\\" + directoryName, s);    //copy the files from root to platform folder
                DirectoryCopy(p.GetRootModules() + "\\" + directoryName, s);    //copy the directories from root to platform folder
                string[] WinRTModulesFiles = Directory.GetFiles(s);
                foreach (string s1 in WinRTModulesFiles)    //Iterate through the directories in WinRT Modules
                {
                    if (Path.GetExtension(s1).Equals(".vcxproj") || Path.GetExtension(s1).Equals(".filters"))
                    {
                        p.EditFile(s1, "modules", directoryName, "WinRT");

                    }
                }

            }
            Console.WriteLine("WinRt Modules Ready");

            foreach (string s in WP8ModulesDirectories)
            {
                string directoryName = new DirectoryInfo(s).Name;
                p.CopyFolderFiles(p.GetRootModules() + "\\" + directoryName, s);    //copy the files from root to platform folder
                DirectoryCopy(p.GetRootModules() + "\\" + directoryName, s);    //copy the directories from root to platform folder
                string[] WP8ModulesFiles = Directory.GetFiles(s);
                foreach (string s1 in WP8ModulesFiles)  //Iterate through the directories in WP8 Modules
                {
                    if (Path.GetExtension(s1).Equals(".vcxproj") || Path.GetExtension(s1).Equals(".filters"))
                    {
                        p.EditFile(s1, "modules", directoryName, "WP8");

                    }
                }


            }
            Console.WriteLine("WP8 Modules Ready");
            Console.WriteLine("OPENCV WIN PACKAGE READY FOR USE :)");


            Console.Read();

        }


        string IFileProcess.GetRoot() // Get Root of opencv-winrt
        {
            return root;
        }
        string IFileProcess.GetRoot3rdParty() // Get Root of opencv-winrt
        {
            return root + "\\3rdparty";
        }
        string IFileProcess.GetRootModules() // Get Root of opencv-winrt
        {
            return root + "\\modules";
        }
        string IFileProcess.GetWinRT3rdParty()
        {
            return root + "\\binWinRt\\3rdparty";
        }
        string IFileProcess.GetWinRTModules()
        {
            return root + "\\binWinRT\\modules";
        }
        string IFileProcess.GetWP83rdParty()
        {
            return root + "\\binWP8\\3rdparty";
        }
        string IFileProcess.GetWP8Modules()
        {
            return root + "\\binWP8\\modules";
        }
        static void DirectoryCopy(string sourceDirName, string destDirName)
        {
            // Get the subdirectories for the specified directory.
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);
            DirectoryInfo[] dirs = dir.GetDirectories();

            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + sourceDirName);
            }

            // If the destination directory doesn't exist, create it. 
            if (!Directory.Exists(destDirName))
            {
                Directory.CreateDirectory(destDirName);
            }

            // Get the files in the directory and copy them to the new location.
            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo file in files)
            {

                string temppath = Path.Combine(destDirName, file.Name);
                file.CopyTo(temppath, true);

            }

            // If copying subdirectories, copy them and their contents to new location. 

            foreach (DirectoryInfo subdir in dirs)
            {
                string temppath = Path.Combine(destDirName, subdir.Name);
                DirectoryCopy(subdir.FullName, temppath);
            }

        }

        void IFileProcess.CopyFolderFiles(string src, string dst) // Get Folder and copy file contents and output to destination
        {
            string sourceDir = src;
            string backupDir = dst;

            try
            {
                string[] files = Directory.GetFiles(sourceDir);


                // Copy picture files. 
                foreach (string f in files)
                {
                    // Remove path from the file name. 
                    string fName = f.Substring(sourceDir.Length + 1);

                    // Use the Path.Combine method to safely append the file name to the path. 
                    // Will overwrite if the destination file already exists.
                    File.Copy(Path.Combine(sourceDir, fName), Path.Combine(backupDir, fName), true);
                }


            }

            catch (DirectoryNotFoundException dirNotFound) // Throw exception if directory is not found.
            {
                Console.WriteLine(dirNotFound.Message);
            }
        }

        void IFileProcess.EditFile(string filePath, string typeOfLib, string lib, string platform)//Read and edit sepcified file
        {
            string[] lines = File.ReadAllLines(filePath);

            //Changes to be made if file belongs to "3rdparty" folder
            if (typeOfLib.Equals("thirdParty"))
            {
                for (int i = 0; i < lines.Length; i++)
                {
                    if (lines[i].Contains("<AdditionalIncludeDirectories>"))
                    {
                        lines[i] = lines[i].Replace("..\\..\\..\\3rdparty\\", "..\\..\\3rdparty\\");
                        lines[i] = lines[i].Replace("..\\..\\..\\bin" + platform, "..\\..\\");
                    }
                    if (lines[i].Contains("<ClInclude Include=" + "\""))
                    {
                        lines[i] = lines[i].Replace("..\\..\\..\\3rdparty\\" + lib + "\\", ".\\");
                    }
                    if (lines[i].Contains("<ClCompile Include=" + "\""))
                    {
                        lines[i] = lines[i].Replace("..\\..\\..\\3rdparty\\" + lib + "\\", ".\\");
                    }
                    if (lines[i].Contains("<ProjectReference Include=" + "\""))
                    {
                        lines[i] = lines[i].Replace("..\\" + lib + "\\", ".\\");
                    }
                    if (lines[i].Contains("CustomBuild Include=" + "\"")) 
                    {
                        lines[i] = lines[i].Replace("..\\..\\..\\3rdparty\\" + lib + "\\", ".\\");
                    }

                }
            }
            //Changes to be made if file belongs to "modules" folder
            if (typeOfLib.Equals("modules"))
            {
                for (int j = 0; j < lines.Length; j++)
                {
                    if (lines[j].Contains("<AdditionalIncludeDirectories>"))
                    {
                        lines[j] = lines[j].Replace("..\\..\\..\\modules\\", "..\\..\\modules\\");
                        lines[j] = lines[j].Replace("..\\..\\..\\3rdparty\\", "..\\..\\3rdparty\\");
                        lines[j] = lines[j].Replace("..\\..\\..\\bin" + platform, "..\\..\\");
                    }
                    if (lines[j].Contains("<ClInclude Include=" + "\""))
                    {                        
                        lines[j] = lines[j].Replace("..\\..\\..\\modules\\" + lib + "\\", ".\\");   
          
                    }
                    if (lines[j].Contains("<ClCompile Include=" + "\""))
                    {
                        lines[j] = lines[j].Replace("..\\..\\..\\modules\\" + lib + "\\", ".\\");

                    }
                    if (lines[j].Contains("<ProjectReference Include=" + "\""))
                    {
                        lines[j] = lines[j].Replace("..\\" + lib + "\\", ".\\");
                    }
                    if (lines[j].Contains("<None Include=" + "\""))
                    {
                        lines[j] = lines[j].Replace("..\\..\\..\\modules\\" + lib + "\\", ".\\");
                        lines[j] = lines[j].Replace("..\\..\\..\\modules\\", "..\\" );

                    }
                    if (lines[j].Contains("CustomBuild Include=" + "\""))
                    {
                        lines[j] = lines[j].Replace("..\\..\\..\\modules\\" + lib + "\\", ".\\");
                    }
                }
            }


            File.WriteAllLines(filePath, lines);

        }

        void IFileProcess.ParseString(string str) // Search for specified string in param and modify it to what you want.
        {

        }

    }
}
